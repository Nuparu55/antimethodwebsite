﻿using Microsoft.EntityFrameworkCore.Migrations;
using MySql.Data.EntityFrameworkCore.Metadata;

namespace AntiMethodWebSite.Migrations
{
    public partial class ExecutorProfession : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Profession_Executor_ExecutorId",
                table: "Profession");

            migrationBuilder.DropIndex(
                name: "IX_Profession_ExecutorId",
                table: "Profession");

            migrationBuilder.DropColumn(
                name: "ExecutorId",
                table: "Profession");

            migrationBuilder.CreateTable(
                name: "ExecutorProf",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySQL:ValueGenerationStrategy", MySQLValueGenerationStrategy.IdentityColumn),
                    ExecutorId = table.Column<int>(nullable: true),
                    ProfessionId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExecutorProf", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExecutorProf_Executor_ExecutorId",
                        column: x => x.ExecutorId,
                        principalTable: "Executor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExecutorProf_Profession_ProfessionId",
                        column: x => x.ProfessionId,
                        principalTable: "Profession",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExecutorProf_ExecutorId",
                table: "ExecutorProf",
                column: "ExecutorId");

            migrationBuilder.CreateIndex(
                name: "IX_ExecutorProf_ProfessionId",
                table: "ExecutorProf",
                column: "ProfessionId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ExecutorProf");

            migrationBuilder.AddColumn<int>(
                name: "ExecutorId",
                table: "Profession",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Profession_ExecutorId",
                table: "Profession",
                column: "ExecutorId");

            migrationBuilder.AddForeignKey(
                name: "FK_Profession_Executor_ExecutorId",
                table: "Profession",
                column: "ExecutorId",
                principalTable: "Executor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
