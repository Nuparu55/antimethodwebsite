﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AntiMethodWebSite.Models
{
    public class Executor
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Discord { get; set; }
        public virtual List<ExecutorProf> Professions { get; set; } 
    }
}
