﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AntiMethodWebSite.Models
{
    public class ExecutorProf
    {
        public int Id { get; set; }
        public virtual Executor Executor { get; set; }
        public virtual Profession Profession { get; set; }
    }
}
