﻿using AntiMethodWebSite.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AntiMethodWebSite.Models
{
    public class Comment
    {
        [Key]
        public int Id { get; set; }
        public virtual AppUser Author { get; set; }
        public virtual Post Post { get; set; }
        public string Content { get; set; }
        public DateTime CreateDate { get; set; }
    }
}
