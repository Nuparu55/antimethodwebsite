﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AntiMethodWebSite.Models
{
    public class Profession
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual List<ExecutorProf> Executors { get; set; }
    }
}
