﻿using AntiMethodWebSite.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace AntiMethodWebSite.Models
{
    public class RatingViewModel
    {
        [DisplayName("Пользователь")]
        public string UserName { get; set; }
        [DisplayName("Anti-Method Points")]
        public int Points { get; set; }
        [DisplayName("Ранг")]
        public Rank Rank { get; set; }
    }
}
