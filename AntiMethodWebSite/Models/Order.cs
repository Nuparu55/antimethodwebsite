﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AntiMethodWebSite.Models
{
    public class Order
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Discord { get; set; }
        public virtual List<OrderItem> OrederItems { get; set; }
        public virtual Profession Profession { get; set; }
        public bool IsCompleted { get; set; }
    }
}
