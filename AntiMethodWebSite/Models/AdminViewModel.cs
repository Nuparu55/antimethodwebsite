﻿using AntiMethodWebSite.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace AntiMethodWebSite.Models
{
    public class AdminViewModel
    {
        public List<AppUser> Users { get; set; }
        public List<Post> Posts { get; set; }
    }
}
