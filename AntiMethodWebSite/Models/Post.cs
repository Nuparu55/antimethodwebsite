﻿using AntiMethodWebSite.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace AntiMethodWebSite.Models
{
    public enum PostType
    {
        [Display(Name = "Новости")]
        News,
        [Display(Name = "Тактики")]
        Tactics,
        [Display(Name = "Логи")]
        Logs,
        [Display(Name = "Прочее")]
        Etc
    }
    public class Post
    {
        [Key]
        public int Id { get; set; }
        public virtual AppUser Author { get; set; }
        [DisplayName("Заголовок")]
        [Required]
        public string Title { get; set; }
        [DisplayName("Текст")]
        [Required]
        public string Content { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? EditDate { get; set; }
        [DisplayName("Тип")]
        public PostType PostType { get; set; }
        public string ImagePath { get; set; }
    }
}
