﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AntiMethodWebSite.Data;
using AntiMethodWebSite.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;
using System.IO;
using Grpc.Core;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;

namespace AntiMethodWebSite.Controllers
{
    public class PostController : Controller
    {
        private UserManager<AppUser> _userManager;
        private SignInManager<AppUser> _signManager;
        private RoleManager<IdentityRole> _roleManager;
        private IHttpContextAccessor _httpContextAccessor;
        private IWebHostEnvironment _appEnvironment;
        private readonly ApplicationDbContext _context;

        private int PageSize = 5;

        public PostController(ApplicationDbContext context, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, RoleManager<IdentityRole> roleManager, IHttpContextAccessor httpContextAccessor, IWebHostEnvironment appEnvironment)
        {
            _context = context;
            _userManager = userManager;
            _signManager = signInManager;
            _roleManager = roleManager;
            _httpContextAccessor = httpContextAccessor;
            _appEnvironment = appEnvironment;
        }

        // GET: Post
        public async Task<IActionResult> Index(int? pageNumber)
        {
            var posts = await GetOrderedPosts().ToListAsync();
            ViewBag.PostType = "";
            ViewBag.Title = "Главная";
            ViewBag.PageSize = PageSize;
            ViewBag.PageNumber = pageNumber ?? 1;
            ViewBag.TotalPages = (int)Math.Ceiling(posts.Count() / (double)PageSize);
            return View(posts.Skip(PageSize * ((pageNumber ?? 1) - 1)).Take(PageSize));
        }

        public async Task<IActionResult> News(int? pageNumber)
        {
            var posts = await GetOrderedPosts().Where(post => post.PostType == PostType.News).ToListAsync();
            ViewBag.PostType = "0";
            ViewBag.Title = "Новости";
            ViewBag.PageSize = PageSize;
            ViewBag.PageNumber = pageNumber ?? 1;
            ViewBag.TotalPages = (int)Math.Ceiling(posts.Count() / (double)PageSize);
            return View("Index", posts.Skip(PageSize * ((pageNumber ?? 1) - 1)).Take(PageSize));
        }

        public async Task<IActionResult> Tactics(int? pageNumber)
        {
            var posts = await GetOrderedPosts().Where(post => post.PostType == PostType.Tactics).ToListAsync();
            ViewBag.PostType = "1";
            ViewBag.Title = "Тактики";
            ViewBag.PageSize = PageSize;
            ViewBag.PageNumber = pageNumber ?? 1;
            ViewBag.TotalPages = (int)Math.Ceiling(posts.Count() / (double)PageSize);
            return View("Index", posts.Skip(PageSize * ((pageNumber ?? 1) - 1)).Take(PageSize));
        }

        public async Task<IActionResult> Logs(int? pageNumber)
        {
            var posts = await GetOrderedPosts().Where(post => post.PostType == PostType.Logs).ToListAsync();
            ViewBag.PostType = "2";
            ViewBag.Title = "Логи";
            ViewBag.PageSize = PageSize;
            ViewBag.PageNumber = pageNumber ?? 1;
            ViewBag.TotalPages = (int)Math.Ceiling(posts.Count() / (double)PageSize);
            return View("Index", posts.Skip(PageSize * ((pageNumber ?? 1) - 1)).Take(PageSize));
        }

        public async Task<IActionResult> Other(int? pageNumber)
        {
            var posts = await GetOrderedPosts().Where(post => post.PostType == PostType.Etc).ToListAsync();
            ViewBag.PostType = "3";
            ViewBag.Title = "Прочее";
            ViewBag.PageSize = PageSize;
            ViewBag.PageNumber = pageNumber ?? 1;
            ViewBag.TotalPages = (int)Math.Ceiling(posts.Count() / (double)PageSize);
            return View("Index", posts.Skip(PageSize * ((pageNumber ?? 1) - 1)).Take(PageSize));
        }

        private IQueryable<Post> GetOrderedPosts()
        {
            return _context.Post.OrderByDescending(p => p.CreationDate);
        }

        // GET: Post/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = await _context.Post
                .FirstOrDefaultAsync(m => m.Id == id);
            if (post == null)
            {
                return NotFound();
            }

            var comments = await _context.Comment.Where(c => c.Post.Id == id).ToListAsync();
            var model = new PostViewModel { Post = post, Comments = comments };

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Comment(string content, int postId)
        {
            var comment = new Comment();
            var post = await _context.Post.FirstOrDefaultAsync(m => m.Id == postId);

            if (post == null)
                return NotFound();

            var user = await _userManager.GetUserAsync(HttpContext.User);
            comment.Content = content;
            comment.Post = post;
            comment.Author = user;
            comment.CreateDate = DateTime.Now;
            _context.Add(comment);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details", new { id = postId });
        }

        public async Task<IActionResult> DeleteComment(int? id, int postId)
        {
            if (id == null)
            {
                return NotFound();
            }

            var comment = await _context.Comment.FirstOrDefaultAsync(m => m.Id == id);
            if (comment == null)
            {
                return NotFound();
            }

            _context.Comment.Remove(comment);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details", new { id = postId });
        }

        // GET: Post/Create
        [Authorize]
        public async Task<IActionResult> Create(string type, string value)
        {
            ViewBag.TypeValue = value ?? "";
            ViewBag.Type = type == "Главная" ? "Выберите тематику" : type;

            var user = await _userManager.GetUserAsync(HttpContext.User);
            if (!await _userManager.IsInRoleAsync(user, "Author"))
                return Forbid();

            return View();
        }

        // POST: Post/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize]
        public async Task<IActionResult> Create([Bind("Id,Title,Content,Author,PostType,CreationDate")] Post post, IFormFile uploadedFile)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.GetUserAsync(HttpContext.User);
                post.Author = user;
                post.CreationDate = DateTime.Now;
                post.ImagePath = AddFile(uploadedFile);
                _context.Add(post);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(post);
        }

        // GET: Post/Edit/5
        [Authorize]
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = await _context.Post.FindAsync(id);
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // POST: Post/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Title,Content,PostType,CreationDate,ImagePath")] Post post, IFormFile uploadedFile)
        {
            if (id != post.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    post.EditDate = DateTime.Now;
                    if (uploadedFile != null && uploadedFile.Length > 0)
                        post.ImagePath = AddFile(uploadedFile);
                    _context.Update(post);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!PostExists(post.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(post);
        }

        // GET: Post/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = await _context.Post
                .FirstOrDefaultAsync(m => m.Id == id);
            if (post == null)
            {
                return NotFound();
            }

            return View(post);
        }

        // POST: Post/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var post = await _context.Post.FindAsync(id);
            _context.Post.Remove(post);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool PostExists(int id)
        {
            return _context.Post.Any(e => e.Id == id);
        }

        [HttpPost]
        public string AddFile(IFormFile uploadedFile)
        {
            if (uploadedFile != null)
            {
                // путь к папке Files
                string path = "/files/" + Path.GetFileNameWithoutExtension(uploadedFile.FileName) + "." + Guid.NewGuid().ToString() + "." + Path.GetExtension(uploadedFile.FileName);
                // сохраняем файл в папку Files в каталоге wwwroot
                using (var fileStream = new FileStream(_appEnvironment.WebRootPath + path, FileMode.Create))
                {
                    uploadedFile.CopyTo(fileStream);
                }
                return path;
            }
            else
                return String.Empty;
        }

        [HttpPost]
        public string UploadFile(IFormFile aUploadedFile)
        {
            var vReturnImagePath = string.Empty;
            if (aUploadedFile.Length > 0)
            {
                var vFileName = Path.GetFileNameWithoutExtension(aUploadedFile.FileName);
                var vExtension = Path.GetExtension(aUploadedFile.FileName);

                string sImageName = vFileName + "." + Guid.NewGuid().ToString();

                var vImageSavePath = _appEnvironment.WebRootPath + "/files/" + sImageName + vExtension;
                //sImageName = sImageName + vExtension;  
                //vReturnImagePath = "/Files/" + sImageName + vExtension;
                ViewBag.Msg = vImageSavePath;
                var path = vImageSavePath;
                vReturnImagePath = "/files/" + sImageName + vExtension;
                // Saving Image in Original Mode  
                using (var fileStream = new FileStream(path, FileMode.Create))
                {
                    aUploadedFile.CopyTo(fileStream);
                }
                var vImageLength = new FileInfo(path).Length;
                //here to add Image Path to You Database ,  
                var successMessage = string.Format("Image was Added Successfully");
            }
            return vReturnImagePath;
        }

        public async void DeleteUser(string email)
        {
            await _userManager.DeleteAsync(await _userManager.FindByEmailAsync(email));
        }

        public async Task<IActionResult> DeletePostManual(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var post = await _context.Post.FindAsync(id);
            var comments = _context.Comment.Where(c => c.Post == post);
            _context.Comment.RemoveRange(comments);
            _context.Post.Remove(post);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
    }
}
