﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AntiMethodWebSite.Data;
using AntiMethodWebSite.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace AntiMethodWebSite.Controllers
{
    public class OrderController : Controller
    {
        private readonly ApplicationDbContext _context;

        public OrderController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult Index()
        {
            return View();
        }

        public async Task<string> AddProfession(string name)
        {
            var prof = new Profession() { Name = name };
            _context.Add(prof);
            await _context.SaveChangesAsync();
            return "Success";
        }

        [HttpPost]
        public async Task<string> RegisterExecutor(string name, string discord, string[] profsRequest)
        {
            var exec = new Executor() { Name = name, Discord = discord };
            _context.Add(exec);
            await _context.SaveChangesAsync();
            foreach (var pr in profsRequest)
            {
                var cntProf = _context.Profession.FirstOrDefault(p => p.Name.ToLower() == pr.ToLower());
                if (cntProf != null)
                {
                    var ep = new ExecutorProf() { Executor = exec, Profession = cntProf };
                    _context.Add(ep);
                }
            }
            await _context.SaveChangesAsync();
            return "Success";
        }

        public string GetExecutors()
        {
            var execs = _context.Executor.ToList();
            var json = JsonConvert.SerializeObject(execs);
            return json;
        }
    }
}
