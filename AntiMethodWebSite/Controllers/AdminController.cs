﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AntiMethodWebSite.Data;
using AntiMethodWebSite.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Http;
using System.IO;
using Grpc.Core;
using Microsoft.AspNetCore.Hosting;
using Newtonsoft.Json;

namespace AntiMethodWebSite.Controllers
{
    public class AdminController : Controller
    {
        private UserManager<AppUser> _userManager;
        private SignInManager<AppUser> _signManager;
        private IHttpContextAccessor _httpContextAccessor;
        private IWebHostEnvironment _appEnvironment;
        private RoleManager<IdentityRole> _roleManager;
        private readonly ApplicationDbContext _context;

        public AdminController(ApplicationDbContext context, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, IHttpContextAccessor httpContextAccessor, IWebHostEnvironment appEnvironment, RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _signManager = signInManager;
            _httpContextAccessor = httpContextAccessor;
            _appEnvironment = appEnvironment;
            _roleManager = roleManager;
        }

        public IActionResult Index()
        {
            return View(GetAdminData);
        }

        [Authorize]
        public async Task<IActionResult> Promote(string name)
        {
            var curUser = await _userManager.GetUserAsync(HttpContext.User);
            if (!await _userManager.IsInRoleAsync(curUser, "Admin"))
                return Forbid();

            var user = await _userManager.FindByNameAsync(name);
            if (user == null)
                return NotFound();

            if (user.Rank < Rank.GuildMaster)
            {
                user.Rank = user.Rank + 1;
                _context.Update(user);
                await _context.SaveChangesAsync();
            }

            return View("Index", GetAdminData);
        }

        [Authorize]
        public async Task<IActionResult> Demote(string name)
        {
            var curUser = await _userManager.GetUserAsync(HttpContext.User);
            if (!await _userManager.IsInRoleAsync(curUser, "Admin"))
                return Forbid();

            var user = await _userManager.FindByNameAsync(name);
            if (user == null)
                return NotFound();

            if (user.Rank > Rank.NotRanked)
            {
                user.Rank = user.Rank - 1;
                _context.Update(user);
                await _context.SaveChangesAsync();
            }

            return View("Index", GetAdminData);
        }

        [Authorize]
        public async Task<IActionResult> AddRole(string name, string role)
        {
            var curUser = await _userManager.GetUserAsync(HttpContext.User);
            if (!await _userManager.IsInRoleAsync(curUser, "Admin"))
                return Forbid();

            var user = await _userManager.FindByNameAsync(name);
            if (user == null)
                return NotFound();

            if (!await _userManager.IsInRoleAsync(user, role))
                await _userManager.AddToRoleAsync(user, role);

            return View("Index", GetAdminData);
        }

        [Authorize]
        public async Task<IActionResult> DeleteRole(string name, string role)
        {
            var curUser = await _userManager.GetUserAsync(HttpContext.User);
            if (!await _userManager.IsInRoleAsync(curUser, "Admin"))
                return Forbid();

            var user = await _userManager.FindByNameAsync(name);
            if (user == null)
                return NotFound();

            if (await _userManager.IsInRoleAsync(user, role))
                await _userManager.RemoveFromRoleAsync(user, role);

            return View("Index", GetAdminData);
        }

        public async Task<IActionResult> InitRoles()
        {
            var role = await _roleManager.FindByNameAsync("Raider");
            if (role == null)
            {
                await _roleManager.CreateAsync(new IdentityRole("Raider"));
            }

            role = await _roleManager.FindByNameAsync("Author");
            if (role == null)
            {
                await _roleManager.CreateAsync(new IdentityRole("Author"));
            }

            role = await _roleManager.FindByNameAsync("Moderator");
            if (role == null)
            {
                await _roleManager.CreateAsync(new IdentityRole("Moderator"));
            }

            role = await _roleManager.FindByNameAsync("Admin");
            if (role == null)
            {
                await _roleManager.CreateAsync(new IdentityRole("Admin"));
            }

            var user = await _userManager.FindByNameAsync("Max");
            if (user != null)
            {
                await _userManager.AddToRoleAsync(user, "Raider");
                await _userManager.AddToRoleAsync(user, "Author");
                await _userManager.AddToRoleAsync(user, "Admin");
            }

            return View("Index", GetAdminData);
        }

        private AdminViewModel GetAdminData
        {
            get
            {
                return new AdminViewModel
                {
                    Posts = _context.Post.OrderByDescending(p => p.CreationDate).ToList(),
                    Users = _context.Users.ToList()

                };
            }
        }
    }
}
