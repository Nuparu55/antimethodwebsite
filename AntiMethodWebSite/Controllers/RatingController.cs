﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using AntiMethodWebSite.Data;
using AntiMethodWebSite.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
namespace AntiMethodWebSite.Controllers
{
    public class RatingController : Controller
    {
        private UserManager<AppUser> _userManager;
        private SignInManager<AppUser> _signManager;
        private RoleManager<IdentityRole> _roleManager;
        private readonly ApplicationDbContext _context;

        public RatingController(ApplicationDbContext context, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, RoleManager<IdentityRole> roleManager)
        {
            _context = context;
            _userManager = userManager;
            _signManager = signInManager;
            _roleManager = roleManager;
        }

        public IActionResult Index()
        {
            var model = new List<RatingViewModel>();
            foreach (var user in _userManager.Users.Where(u => u.Rank >= Rank.Raider).OrderByDescending(u => u.Points).ToList())
            {
                model.Add(new RatingViewModel { UserName = user.UserName, Points = user.Points, Rank = user.Rank });
            }
            return View(model);
        }
    }
}
