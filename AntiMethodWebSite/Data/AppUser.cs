﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
//using AntiMethodWebSite.Data.Migrations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace AntiMethodWebSite.Data
{
    public class AppUser : IdentityUser
    {
        public string NickName { get; set; }
        public int Points { get; set; }
        public Rank Rank { get; set; }
        public string TestStringField { get; set; }
    }

    public enum Rank
    {
        [Display(Name = "Нет Ранга")]
        NotRanked,
        [Display(Name = "Новичок")]
        Novice,
        [Display(Name = "Рейдер")]
        Raider,
        [Display(Name = "Рейд Лидер")]
        RaidLeader,
        [Display(Name = "Гильд Мастер")]
        GuildMaster
    }
}
